import gql from 'graphql-tag';

export const FETCH_CHARACTERS = gql`
    query Characters($page: Int, $searchText: String) {
        characters(page: $page, filter: { name: $searchText }) {
            info {
                count
                next
                prev
                pages
            }
            results {
                id
                name
                image
                status
                species
                gender
            }
        }
    }
`;
