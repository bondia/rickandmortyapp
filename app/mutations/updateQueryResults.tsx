export default function updateQueryResults(prevRes, nextRes) {
    // return new results if no previous
    if (prevRes === null) {
        return nextRes;
    }

    // return previous results if returned empty new results
    if (
        !nextRes ||
        nextRes.characters.results == null ||
        nextRes.characters.results.length === 0
    ) {
        return prevRes;
    }

    // merge results (important for pagination)
    return Object.assign({}, prevRes, {
        characters: {
            info: {
                ...nextRes.characters.info,
                __typename: nextRes.characters.info.__typename
            },
            results: [
                ...prevRes.characters.results,
                ...nextRes.characters.results
            ],
            __typename: nextRes.characters.__typename
        }
    });
}
