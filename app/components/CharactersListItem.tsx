import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, Image, Text } from 'react-native';

import { CharacterInterface } from '../types';

interface Props {
    item: CharacterInterface;
}

class CharactersListItem extends PureComponent<Props> {
    static propTypes = {
        item: PropTypes.object.isRequired
    };

    // Display red border for dead characters
    private decideImageStyles(item) {
        if (item.status === 'Dead') {
            return [styles.image, styles.imageDeadChar];
        }
        return styles.image;
    }

    render() {
        const { item } = this.props;
        return (
            <View style={styles.mainContainer}>
                <Image
                    style={this.decideImageStyles(item)}
                    source={{ uri: item.image }}
                />

                <View style={styles.textContainer}>
                    <Text style={styles.nameText}>{item.name}</Text>
                    <Text style={styles.genderText}>
                        {item.species || "--"} | {item.gender  || "--"} | {item.status  || "--"}
                    </Text>
                </View>
            </View>
        );
    }
}

export default CharactersListItem;

const styles = StyleSheet.create({
    mainContainer: {
        margin: 10,
        flex: 1,
        flexDirection: 'row',
        flexWrap: 'wrap'
    },
    image: {
        borderWidth: 5,
        borderColor: 'green',
        width: '35%',
        aspectRatio: 1
    },

    imageDeadChar: {
        borderColor: 'red'
    },

    textContainer: {
        flex: 1,
        flexWrap: 'wrap'
    },
    nameText: {
        marginTop: 10,
        textAlign: 'center',
        fontSize: 22
    },
    genderText: {
        color: 'grey',
        marginTop: 10,
        textAlign: 'center',
        fontSize: 14
    }
});
