import React, { PureComponent } from 'react';
import { Text, View, TextInput, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';
import _ from 'lodash';

import { FetchResultsInfoType } from '../types';

interface Props {
    info: FetchResultsInfoType;
    onChangeSearch: (text: string) => void;
}

class CharactersListHeader extends PureComponent<Props> {
    static propTypes = {
        info: PropTypes.object.isRequired,
        onChangeSearch: PropTypes.func.isRequired
    };

    // Avoid calling too often the search by debouncing
    private getDelayedOnChangeText() {
        return _.debounce(this.props.onChangeSearch, 500);
    }

    render() {
        const { info, onChangeSearch } = this.props;
        return (
            <View style={styles.mainContainer}>
                <TextInput
                    style={styles.textInput}
                    onChangeText={this.getDelayedOnChangeText()}
                />
                <View style={styles.legendContainer}>
                    <Text style={styles.legendText}>
                        Items {info.count || '0'}
                    </Text>
                    <Text style={styles.legendText}>
                        Pages {info.pages || '0'}
                    </Text>
                    <Text style={styles.legendText}>
                        Next Page {info.next || '--'}
                    </Text>
                </View>
            </View>
        );
    }
}

export default CharactersListHeader;

const styles = StyleSheet.create({
    mainContainer: {
        height: 100
    },
    textInput: {
        height: 40,
        borderColor: 'gray',
        borderWidth: 1,
        margin: 5
    },
    legendContainer: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
    legendText: {
        margin: 10
    }
});
