import React, { PureComponent } from 'react';
import { FlatList } from 'react-native';
import PropTypes from 'prop-types';

import { CharacterInterface } from '../types';

import CharactersListItem from './CharactersListItem';

interface Props {
    items: Array<CharacterInterface>;
    searchText: String;
    onEndReached: (any) => void;
}

class CharactersList extends PureComponent<Props> {
    static propTypes = {
        searchText: PropTypes.string.isRequired,
        items: PropTypes.array,
        onEndReached: PropTypes.func.isRequired
    };

    // needed reference for scrolling up
    private listRef = React.createRef<FlatList<any>>();

    componentDidUpdate(prevProps) {
        // scroll up when search text changes
        if (this.props.searchText != prevProps.searchText) {
            this.listRef.current.scrollToOffset({ offset: 0, animated: false });
        }
    }

    render() {
        const { items, onEndReached } = this.props;
        return (
            <FlatList
                ref={this.listRef}
                data={items}
                keyExtractor={(item: CharacterInterface) => item.id.toString()}
                renderItem={({ item }) => <CharactersListItem item={item} />}
                onEndReached={onEndReached}
                onEndReachedThreshold={0.5}
            />
        );
    }
}

export default CharactersList;
