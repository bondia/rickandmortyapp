import React, { ComponentType } from 'react';
import { graphql } from 'react-apollo';
import RawComponent from './RawComponent';

import { FETCH_CHARACTERS } from '../../apolloDefinitions';

export default graphql(FETCH_CHARACTERS, {
    options: {
        variables: {
            page: 1,
            searchText: ''
        }
    }
})(RawComponent as ComponentType<any>);
