import updateQueryResults from '../../mutations/updateQueryResults';

interface State {
    page: number;
    searchText: string;
    loadingSearch: boolean;
    loadingPage: boolean;
}

export default function updateSearch(
    previousResult: object,
    fetchMoreResult: object,
    oldSearchText: string,
    state: State,
    setState: Function
) {
    const { loadingSearch, searchText } = state;
    // discard if search text changed in between
    // also make sure that the search is still loading, might be solved with another call
    if (loadingSearch && oldSearchText != searchText) {
        return previousResult;
    }
    // reset flags
    setState({ loadingPage: false, loadingSearch: false });
    // decide results
    return updateQueryResults(null, fetchMoreResult);
}
