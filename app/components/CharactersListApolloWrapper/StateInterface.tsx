export default interface StateInterface {
    page: number;
    searchText: string;
    loadingSearch: boolean;
    loadingPage: boolean;
}
