import React, { PureComponent } from 'react';
import { View, Text, StyleSheet, ActivityIndicator } from 'react-native';
import PropTypes from 'prop-types';

import PropsInterface from './PropsInterface';
import StateInterface from './stateInterface';
import updateSearch from './updateSearch';
import updatePagination from './updatePagination';

import CharactersListHeader from '../CharactersListHeader';
import CharactersList from '../CharactersList';

/*
 * Apollo wrapper for loading data
 * This component loads the data and performs logic to decide
 * which data should be rendered.
 *
 * Because of a bug with apollo client library I needed to use some
 * flags to avoid rendering duplicated data or old calls data.
 * https://github.com/apollographql/apollo-client/issues/2499
 *
 * This bug makes impossible to know with which variables the fetchMore request
 * was called.
 *
 * The solution I had to come up is quite problematic in order to make proper testing.
 *
 * Also I think we could have some issues when apollo answers really slow.
 */
class RawComponent extends PureComponent<PropsInterface, StateInterface> {
    static propTypes = {
        data: PropTypes.object.isRequired
    };

    constructor(props) {
        super(props);
        this.state = {
            page: 1,
            searchText: '',
            loadingSearch: false,
            loadingPage: false
        };
    }

    private onChangeSearch(searchText: string) {
        const { loadingSearch, loadingPage } = this.state;
        const { fetchMore } = this.props.data;

        // update state
        this.setState({
            page: 1,
            searchText,
            loadingSearch: true,
            loadingPage: false
        });

        // expose component
        const self = this;
        const setState = state => self.setState(state);
        const state = this.state;
        // prepare update query with exposed paramters
        const updateQuery = (previousResult, { fetchMoreResult }) =>
            updateSearch(
                previousResult,
                fetchMoreResult,
                searchText,
                state,
                setState
            );

        // fetch more
        fetchMore({
            variables: {
                page: 1,
                searchText
            },
            updateQuery
        });
    }

    private onEndReached() {
        const { searchText, loadingSearch, loadingPage } = this.state;
        const { fetchMore, characters } = this.props.data;
        const { info } = characters;

        // avoid collisions
        if (loadingSearch || loadingPage) {
            return;
        }
        // enable collisions flag
        this.setState({ loadingPage: true });

        // expose component
        const self = this;
        const setState = state => self.setState(state);
        const state = this.state;
        // prepare update query with exposed paramters
        const updateQuery = (previousResult, { fetchMoreResult, variables }) =>
            updatePagination(
                previousResult,
                fetchMoreResult,
                variables.page,
                searchText,
                state,
                setState
            );

        // fetch more
        fetchMore({
            variables: {
                page: info.next,
                searchText
            },
            updateQuery
        });
    }

    render() {
        const { data } = this.props;
        const { searchText } = this.state;

        // loading spinner
        if (data.networkStatus === 1) {
            return <ActivityIndicator style={styles.loader} size="large" />;
        }

        // load error
        if (data.error) {
            return <Text>Error: {data.error.message}</Text>;
        }

        return (
            <View style={styles.container}>
                <CharactersListHeader
                    info={data.characters.info}
                    onChangeSearch={searchText =>
                        this.onChangeSearch(searchText)
                    }
                />
                <CharactersList
                    searchText={searchText}
                    items={data.characters.results}
                    onEndReached={() => this.onEndReached()}
                />
            </View>
        );
    }
}

export default RawComponent;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: '100%'
    },
    loader: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        width: '100%',
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center'
    }
});
