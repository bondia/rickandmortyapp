import updateQueryResults from '../../mutations/updateQueryResults';

interface State {
    page: number;
    searchText: string;
    loadingSearch: boolean;
    loadingPage: boolean;
}

export default function updatePagination(
    prevRes: object,
    newRes: object,
    newPage: number,
    oldSearchText: string,
    state: State,
    setState: Function
) {
    const { loadingSearch, page, searchText } = state;
    // avoid collisions
    // - when loading a new search
    // - when the search is different
    // - the page loaded is previous as the current one
    if (loadingSearch || searchText != oldSearchText || newPage <= page) {
        return prevRes;
    }
    // update state and reset flags
    setState({ page: newPage, loadingPage: false });
    return updateQueryResults(prevRes, newRes);
}
