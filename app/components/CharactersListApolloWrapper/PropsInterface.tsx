import StateInterface from './stateInterface';
import { CharacterInterface, FetchResultsInfoType } from '../../types';

export default interface Props {
    data: {
        characters: {
            info: FetchResultsInfoType;
            results: Array<CharacterInterface>;
        };
        variables: StateInterface;
        fetchMore: Function;
        networkStatus: Number;
        error: {
            message: String;
        };
    };
}
