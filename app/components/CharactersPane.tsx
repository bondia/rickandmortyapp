import React, { PureComponent } from 'react';
import { StyleSheet, View } from 'react-native';

import CharactersListApolloWrapper from './CharactersListApolloWrapper';

interface Props {}

class CharactersPane extends PureComponent<Props> {
    render() {
        return (
            <View style={styles.container}>
                <CharactersListApolloWrapper />
            </View>
        );
    }
}

export default CharactersPane;

const styles = StyleSheet.create({
    container: {
        paddingTop: 50,
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    }
});
