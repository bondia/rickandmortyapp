type FetchInfoType = {
    count: number;
    next: number;
    prev: number;
    pages: number;
};

export default FetchInfoType;
