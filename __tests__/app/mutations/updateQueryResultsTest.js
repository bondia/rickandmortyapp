import updateQueryResults from '../../../app/mutations/updateQueryResults';

describe('updateQueryResults', () => {
    it('Check output', () => {
        let prevResults = { a: "asdf" };
        let nextResults = { b: "eeee" };

        // recieve nextResults if prevResutls are null
        expect(updateQueryResults(null, nextResults)).toBe(nextResults);
        // recieve prevResutls if nextResults are null
        expect(updateQueryResults(prevResults, null)).toBe(prevResults);

        // recieve prevResutls if no characters recieved
        prevResults = { a: "asdf" };
        nextResults = { characters: { results: null } };
        expect(updateQueryResults(prevResults, nextResults)).toBe(prevResults);
        prevResults = { a: "asdf" };
        nextResults = { characters: { results: [] } };
        expect(updateQueryResults(prevResults, nextResults)).toBe(prevResults);

        // merge array
        prevResults = {
            characters: {
                info: {
                    prop1: 'a',
                    prop2: 'b',
                    prop3: 'c',
                    __typename: 'my_type'
                },
                results: [
                    {
                        a: 'uno'
                    },
                    {
                        a: 'dos'
                    }
                ],
                __typename: 'my_type_2'
            }
        };
        nextResults = {
            characters: {
                info: {
                    prop1: 'd',
                    prop2: 'e',
                    prop3: 'f',
                    __typename: 'my_type_3'
                },
                results: [
                    {
                        a: 'tres'
                    },
                    {
                        a: 'cuatro'
                    }
                ],
                __typename: 'my_type_4'
            }
        };
        const result = {
            characters: {
                info: {
                    prop1: 'd',
                    prop2: 'e',
                    prop3: 'f',
                    __typename: 'my_type_3'
                },
                results: [
                    {
                        a: 'uno'
                    },
                    {
                        a: 'dos'
                    },
                    {
                        a: 'tres'
                    },
                    {
                        a: 'cuatro'
                    }
                ],
                __typename: 'my_type_4'
            }
        };

        expect(updateQueryResults(prevResults, nextResults)).toStrictEqual(
            result
        );
    });
});
