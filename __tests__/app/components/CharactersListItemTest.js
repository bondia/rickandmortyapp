import React from 'react';
import { Text, Image } from 'react-native';
import renderer from 'react-test-renderer';

import CharactersListItem from '../../../app/components/CharactersListItem';

describe('CharactersListItem', () => {
    it('Render alive character', () => {
        const item = {
            image: 'dummy.jpg',
            name: 'My Name',
            species: 'cat',
            gender: 'none',
            status: 'sick'
        };

        const testRenderer = renderer.create(
            <CharactersListItem item={item} />
        );
        expect(testRenderer.toJSON()).toMatchSnapshot();

        const testInstance = testRenderer.root;
        // Find Image
        const imageElement = testInstance.findByType(Image);
        expect(imageElement.props.source.uri).toBe('dummy.jpg');

        // Find text elements
        const textElements = testInstance.findAllByType(Text);
        let foundName = false;
        let foundStats = false;
        for (var i = textElements.length - 1; i >= 0; i--) {
            const child = textElements[i].props.children;
            if (
                Array.isArray(child) &&
                child.join('') === 'cat | none | sick'
            ) {
                expect(child.join('')).toBe('cat | none | sick');
                foundStats = true;
            } else if (child === 'My Name') {
                expect(child).toBe('My Name');
                foundName = true;
            }
        }

        expect(foundName).toBe(true);
        expect(foundStats).toBe(true);
    });

    it('Render dead character', () => {
        const item = {
            image: 'dummy2.jpg',
            name: 'My Name2',
            species: 'cat2',
            gender: 'none2',
            status: 'Dead'
        };

        const testRenderer = renderer.create(
            <CharactersListItem item={item} />
        );
        expect(testRenderer.toJSON()).toMatchSnapshot();

        const testInstance = testRenderer.root;
        // Find Image
        const imageElement = testInstance.findByType(Image);
        expect(imageElement.props.source.uri).toBe('dummy2.jpg');

        // Find text elements
        const textElements = testInstance.findAllByType(Text);
        let foundName = false;
        let foundStats = false;
        for (var i = textElements.length - 1; i >= 0; i--) {
            const child = textElements[i].props.children;
            if (
                Array.isArray(child) &&
                child.join('') === 'cat2 | none2 | Dead'
            ) {
                expect(child.join('')).toBe('cat2 | none2 | Dead');
                foundStats = true;
            } else if (child === 'My Name2') {
                expect(child).toBe('My Name2');
                foundName = true;
            }
        }

        expect(foundName).toBe(true);
        expect(foundStats).toBe(true);
    });

    it('Render empty character', () => {
        const item = {};

        const testRenderer = renderer.create(
            <CharactersListItem item={item} />
        );
        expect(testRenderer.toJSON()).toMatchSnapshot();

        const testInstance = testRenderer.root;
        // Find Image
        const imageElement = testInstance.findByType(Image);
        expect(imageElement.props.source.uri).toBe();

        // Find text elements
        const textElements = testInstance.findAllByType(Text);
        let foundName = false;
        let foundStats = false;
        for (var i = textElements.length - 1; i >= 0; i--) {
            const child = textElements[i].props.children;
            if (Array.isArray(child) && child.join('') === '-- | -- | --') {
                expect(child.join('')).toBe('-- | -- | --');
                foundStats = true;
            } else if (child === undefined) {
                expect(child).toBe();
                foundName = true;
            }
        }

        expect(foundName).toBe(true);
        expect(foundStats).toBe(true);
    });
});
