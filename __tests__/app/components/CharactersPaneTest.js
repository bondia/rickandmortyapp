import React from 'react';
import { Text } from 'react-native';
import renderer from 'react-test-renderer';

describe('CharactersPane', () => {
    beforeEach(() => {
        jest.mock(
            '../../../app/components/CharactersListApolloWrapper',
            () => () => <Text>CharactersListApolloWrapperMock</Text>
        );
    });

    it('Render test', () => {
        const Pane = require('../../../app/components/CharactersPane').default;

        const testRenderer = renderer.create(<Pane />);

        // check snapshot
        expect(testRenderer.toJSON()).toMatchSnapshot();

        // check inner component
        const testInstance = testRenderer.root;
        const textInstance = testInstance.findByType(Text);
        expect(textInstance.props.children).toBe(
            'CharactersListApolloWrapperMock'
        );
    });
});
