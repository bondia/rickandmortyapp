import React from 'react';
import { Text, TextInput } from 'react-native';
import renderer from 'react-test-renderer';

import CharactersListHeader from '../../../app/components/CharactersListHeader';

describe('CharactersListHeader', () => {
    it('Render with info', () => {
        const info = {
            count: 10,
            pages: 2,
            next: 2
        };

        const testRenderer = renderer.create(
            <CharactersListHeader info={info} onChangeSearch={() => {}} />
        );
        expect(testRenderer.toJSON()).toMatchSnapshot();

        const testInstance = testRenderer.root;
        // Find text input
        const textInputElement = testInstance.findByType(TextInput);
        expect(textInputElement).toBeDefined();

        // Find text elements
        const textElements = testInstance.findAllByType(Text);
        let foundItems = false;
        let foundPages = false;
        let foundNext = false;
        for (var i = textElements.length - 1; i >= 0; i--) {
            const child = textElements[i].props.children.join('');
            if (child === 'Items 10') {
                expect(child).toBe('Items 10');
                foundItems = true;
            }

            if (child === 'Pages 2') {
                expect(child).toBe('Pages 2');
                foundPages = true;
            }

            if (child === 'Next Page 2') {
                expect(child).toBe('Next Page 2');
                foundNext = true;
            }
        }

        expect(foundItems).toBe(true);
        expect(foundPages).toBe(true);
        expect(foundNext).toBe(true);
    });

    it('Render with empty info', () => {
        const info = {
            count: null,
            pages: null,
            next: null
        };

                const testRenderer = renderer.create(
            <CharactersListHeader info={info} onChangeSearch={() => {}} />
        );
        expect(testRenderer.toJSON()).toMatchSnapshot();

        const testInstance = testRenderer.root;
        // Find text input
        const textInputElement = testInstance.findByType(TextInput);
        expect(textInputElement).toBeDefined();

        // Find text elements
        const textElements = testInstance.findAllByType(Text);
        let foundItems = false;
        let foundPages = false;
        let foundNext = false;
        for (var i = textElements.length - 1; i >= 0; i--) {
            const child = textElements[i].props.children.join('');
            if (child === 'Items 0') {
                expect(child).toBe('Items 0');
                foundItems = true;
            }

            if (child === 'Pages 0') {
                expect(child).toBe('Pages 0');
                foundPages = true;
            }

            if (child === 'Next Page --') {
                expect(child).toBe('Next Page --');
                foundNext = true;
            }
        }

        expect(foundItems).toBe(true);
        expect(foundPages).toBe(true);
        expect(foundNext).toBe(true);
    });
});
