import React from 'react';
import { ActivityIndicator, Text } from 'react-native';
import renderer from 'react-test-renderer';

jest.useFakeTimers();

describe('CharactersListApolloWrapper', () => {
    // usefull to trigger events
    let headerMock = null;
    let listMock = null;

    const updateQueryResultsMock = jest.fn();
    updateQueryResultsMock.mockReturnValue('updateQueryResults return');

    beforeEach(() => {
        jest.mock(
            '../../../app/components/CharactersListHeader',
            () => props => {
                function CharactersListHeaderMock(props) {
                    return <Text>CharactersListHeaderMock</Text>;
                }
                headerMock = <CharactersListHeaderMock {...props} />;
                return headerMock;
            }
        );
        jest.mock('../../../app/components/CharactersList', () => props => {
            function CharactersListMock(props) {
                return <Text>CharactersListMock</Text>;
            }
            listMock = <CharactersListMock {...props} />;
            return listMock;
        });

        jest.mock(
            '../../../app/mutations/updateQueryResults',
            () => updateQueryResultsMock
        );
    });

    it('Render - loading', () => {
        const RawComponent = require('../../../app/components/CharactersListApolloWrapper')
            .RawComponent;

        const data = {
            characters: {
                info: null,
                results: null
            },
            fetchMore: () => {},
            networkStatus: 1,
            error: null
        };

        const testRenderer = renderer.create(
            <RawComponent data={data} />
        );

        // check snapshot
        expect(testRenderer.toJSON()).toMatchSnapshot();

        // check loading component
        const testInstance = testRenderer.root;
        expect(
            testInstance.findByType(ActivityIndicator).type.displayName
        ).toBe('ActivityIndicator');
    });

    it('Render - error', () => {
        const RawComponent = require('../../../app/components/CharactersListApolloWrapper')
            .RawComponent;

        const data = {
            characters: {
                info: null,
                results: null
            },
            fetchMore: () => {},
            networkStatus: 0,
            error: {
                message: 'Here my error'
            }
        };

        const testRenderer = renderer.create(
            <RawComponent data={data} />
        );
        const testInstance = testRenderer.root;

        // check snapshot
        expect(testRenderer.toJSON()).toMatchSnapshot();

        // check error message is displayed
        const textRenderer = testInstance.findByType(Text);
        expect(textRenderer.props.children.join('')).toBe(
            'Error: Here my error'
        );
    });

    it('Render - Normal render', () => {
        const RawComponent = require('../../../app/components/CharactersListApolloWrapper')
            .RawComponent;

        const data = {
            characters: {
                info: null,
                results: null
            },
            fetchMore: () => {},
            networkStatus: 0,
            error: null
        };

        const testRenderer = renderer.create(
            <RawComponent data={data} />
        );

        // check snapshot
        expect(testRenderer.toJSON()).toMatchSnapshot();

        // Find text elements
        const testInstance = testRenderer.root;
        const textElements = testInstance.findAllByType(Text);
        let foundHeader = false;
        let foundList = false;
        for (var i = textElements.length - 1; i >= 0; i--) {
            const child = textElements[i].props.children;
            if (child === 'CharactersListHeaderMock') {
                expect(child).toBe('CharactersListHeaderMock');
                foundHeader = true;
            }
            if (child === 'CharactersListMock') {
                expect(child).toBe('CharactersListMock');
                foundList = true;
            }
        }

        expect(foundHeader).toBe(true);
        expect(foundList).toBe(true);
    });

    it('On change search', () => {
        const RawComponent = require('../../../app/components/CharactersListApolloWrapper')
            .RawComponent;

        let data = {
            characters: {
                info: {
                    count: 100,
                    next: 2,
                    prev: 1,
                    pages: 5
                },
                results: null
            },
            fetchMore: jest.fn(),
            networkStatus: 0,
            error: null
        };

        const testRenderer = renderer.create(
            <RawComponent data={data} />
        );

        const calls = data.fetchMore.mock.calls;

        // simulate pagination
        listMock.props.onEndReached();
        expect(calls.length).toBe(1);
        expect(calls[0][0].variables.page).toBe(2);
        expect(calls[0][0].variables.searchText).toBe('');

        // simulate search - should reset pagination
        headerMock.props.onChangeSearch('new search');
        expect(calls.length).toBe(2);
        expect(calls[1][0].variables.page).toBe(1);
        expect(calls[1][0].variables.searchText).toBe('new search');

        // can't paginate when search is happening
        listMock.props.onEndReached();
        expect(calls.length).toBe(2);

        // can't paginate when pagination is already happening
        testRenderer.root.instance.setState({
            page: 1,
            searchText: 'asdf',
            loadingSearch: false,
            loadingPage: true
        });
        listMock.props.onEndReached();
        expect(calls.length).toBe(2);
    });

    it('test updateSearch', () => {
        const updateSearch = require('../../../app/components/CharactersListApolloWrapper')
            .updateSearch;

        const setStateMock = jest.fn();
        let prevResults = { a: 'asdf' };
        let nextResults = { b: 'eeee' };

        // do not update when loading new search
        let state = {
            loadingSearch: true,
            searchText: 'acapiporla'
        };
        let output = updateSearch(
            prevResults,
            nextResults,
            'acapiporla 2',
            state,
            setStateMock
        );
        expect(output).toBe(prevResults);
        expect(setStateMock.mock.calls.length).toBe(0);

        // // update
        state = {
            loadingSearch: true,
            searchText: 'acapiporla'
        };
        output = updateSearch(
            prevResults,
            nextResults,
            'acapiporla',
            state,
            setStateMock
        );
        expect(output).toBe('updateQueryResults return');
        expect(setStateMock.mock.calls.length).toBe(1);
        expect(setStateMock).toHaveBeenCalledWith({ loadingPage: false, loadingSearch: false });
        expect(updateQueryResultsMock.mock.calls.length).toBe(1);
        expect(updateQueryResultsMock).toHaveBeenCalledWith(null, nextResults);
    });

    it('test updatePagination', () => {
        const updatePagination = require('../../../app/components/CharactersListApolloWrapper')
            .updatePagination;
        updateQueryResultsMock.mockClear();

        const setStateMock = jest.fn();
        let prevResults = { a: 'asdf' };
        let nextResults = { b: 'eeee' };

        // do not update when loading new search
        let state = {
            loadingSearch: true,
            page: 3,
            searchText: 'acapiporla'
        };
        let output = updatePagination(
            prevResults,
            nextResults,
            4,
            'acapiporla',
            state,
            setStateMock
        );
        expect(output).toBe(prevResults);
        expect(setStateMock.mock.calls.length).toBe(0);

        // do not update when search is different
        state = {
            loadingSearch: false,
            page: 3,
            searchText: 'acapiporla'
        };
        output = updatePagination(
            prevResults,
            nextResults,
            4,
            'acapiporla 2',
            state,
            setStateMock
        );
        expect(output).toBe(prevResults);
        expect(setStateMock.mock.calls.length).toBe(0);
        expect(output).toBe(prevResults);

        // do not update when page is smaller
        state = {
            loadingSearch: false,
            page: 3,
            searchText: 'acapiporla'
        };
        output = updatePagination(
            prevResults,
            nextResults,
            2,
            'acapiporla',
            state,
            setStateMock
        );
        expect(output).toBe(prevResults);
        expect(setStateMock.mock.calls.length).toBe(0);
        expect(output).toBe(prevResults);

        // update
        state = {
            loadingSearch: false,
            page: 3,
            searchText: 'acapiporla'
        };
        output = updatePagination(
            prevResults,
            nextResults,
            4,
            'acapiporla',
            state,
            setStateMock
        );


        expect(output).toBe('updateQueryResults return');
        expect(setStateMock.mock.calls.length).toBe(1);
        expect(setStateMock).toHaveBeenCalledWith({ page: 4, loadingPage: false });
        expect(updateQueryResultsMock.mock.calls.length).toBe(1);
        expect(updateQueryResultsMock).toHaveBeenCalledWith(prevResults, nextResults);
    });
});
