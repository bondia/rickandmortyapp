import React, { PureComponent } from 'react';
import { FlatList, Text } from 'react-native';
import renderer from 'react-test-renderer';

jest.useFakeTimers();

describe('CharactersList', () => {
    beforeEach(() => {
        jest.mock('../../../app/components/CharactersListItem', () => () => (
            <Text>Mocked</Text>
        ));
    });

    it('Render 2 items', () => {
        const CharactersList = require('../../../app/components/CharactersList')
            .default;
        const items = [
            {
                id: 1,
                image: 'dummy.jpg',
                name: 'My Name',
                species: 'cat',
                gender: 'none',
                status: 'sick'
            },
            {
                id: 2,
                image: 'dummy2.jpg',
                name: 'My Name 2',
                species: 'cat 2',
                gender: 'none 2',
                status: 'sick 2'
            }
        ];

        const testRenderer = renderer.create(
            <CharactersList
                items={items}
                searchText="dummy text"
                onEndReached={() => {}}
            />
        );

        // check snapshot
        expect(testRenderer.toJSON()).toMatchSnapshot();

        // count items
        const testInstance = testRenderer.root;
        const listElement = testInstance.findByType(FlatList);
        expect(listElement.props.data.length).toBe(2);
    });

    it('Render 3 items', () => {
        const CharactersList = require('../../../app/components/CharactersList')
            .default;
        const items = [
            {
                id: 1,
                image: 'dummy.jpg',
                name: 'My Name',
                species: 'cat',
                gender: 'none',
                status: 'sick'
            },
            {
                id: 2,
                image: 'dummy2.jpg',
                name: 'My Name 2',
                species: 'cat 2',
                gender: 'none 2',
                status: 'sick 2'
            },
            {
                id: 3,
                image: 'dummy3.jpg',
                name: 'My Name 3',
                species: 'cat 3',
                gender: 'none 3',
                status: 'sick 3'
            }
        ];

        const testRenderer = renderer.create(
            <CharactersList
                items={items}
                searchText="dummy text"
                onEndReached={() => {}}
            />
        );

        // check snapshot
        expect(testRenderer.toJSON()).toMatchSnapshot();

        // count items
        const testInstance = testRenderer.root;
        const listElement = testInstance.findByType(FlatList);
        expect(listElement.props.data.length).toBe(3);
    });

    it('Render 0 items', () => {
        const CharactersList = require('../../../app/components/CharactersList')
            .default;
        const items = [];

        const testRenderer = renderer.create(
            <CharactersList
                items={items}
                searchText="dummy text"
                onEndReached={() => {}}
            />
        );

        // check snapshot
        expect(testRenderer.toJSON()).toMatchSnapshot();

        // count items
        const testInstance = testRenderer.root;
        const listElement = testInstance.findByType(FlatList);
        expect(listElement.props.data.length).toBe(0);
    });

    it('Component did update', () => {
        const CharactersList = require('../../../app/components/CharactersList')
            .default;

        let items = [];
        for (var i = 0; i < 25; i++) {
            items.push({
                id: i + 1,
                image: 'dummy.jpg',
                name: 'My Name',
                species: 'cat',
                gender: 'none',
                status: 'sick'
            });
        }

        const testRenderer = renderer.create(
            <CharactersList
                items={items}
                searchText="dummy text"
                onEndReached={() => {}}
            />
        );

        // count items
        const testInstance = testRenderer.root;
        const listElement = testInstance.findByType(FlatList);
        expect(listElement.props.data.length).toBe(25);

        // add mock
        const scrollToOffsetMock = jest.fn();
        listElement.instance.scrollToOffset = scrollToOffsetMock;

        testRenderer.update(
            <CharactersList
                items={items}
                searchText="dummy text 2"
                onEndReached={() => {}}
            />
        );

        testRenderer.update(
            <CharactersList
                items={[]}
                searchText="dummy text 2"
                onEndReached={() => {}}
            />
        );

        testRenderer.update(
            <CharactersList
                items={items}
                searchText="dummy text 3"
                onEndReached={() => {}}
            />
        );

        expect(scrollToOffsetMock.mock.calls.length).toBe(2);
        scrollToOffsetMock.mockReset();
    });
});
