# Rick and Morty APP

## Run Application

### Install dependencies and run node

`
npm install
npm start
`

### Run IOS
Before running your app on iOS, make sure you have CocoaPods installed and initialize the project.

`
pod install
npm run ios
`

### Unit tests

`
npm test
`

### Update snapshots tests
Testing is using also snapshots so from time to time will be needed to update them.

`
test-update-snapshot
`

### Run Linter

`
npm run lint
`

## Project information

### Rick and Morty API

https://rickandmortyapi.com/documentation/#graphql
https://rickandmortyapi.com/graphql/

## Apollo

To fetch data from the server I chose to use GraphQL.
To do that I used Apollo client because of simplicity.
https://www.apollographql.com/docs/react/

Last day of development; when testing the whole application I found a bug due to a bug in the Apollo library.
I could solve it, but I believe that it could still be buggy on extreme situations where the connection is really slow.
Here the report on the library bug:
https://github.com/apollographql/apollo-client/issues/2499
Due to the bug; becomes really complicated to know to which request belongs every response.

Probably I would now switch the library with another one.
