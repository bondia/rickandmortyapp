import React from 'react';
import ApolloClient from 'apollo-boost';
import { ApolloProvider } from 'react-apollo';

import CharactersPane from './app/components/CharactersPane';

const client = new ApolloClient({
    uri: 'https://rickandmortyapi.com/graphql'
});

interface Props {};

export default function App<Props>() {
    return (
        <ApolloProvider client={client}>
            <CharactersPane />
        </ApolloProvider>
    );
}
